CREATE TABLE IF NOT EXISTS notifications
(
    id                  BIGSERIAL PRIMARY KEY,
    user_id             BIGINT NOT NULL,
    read                BOOLEAN   DEFAULT FALSE,
    seen                BOOLEAN   DEFAULT FALSE,
    type_id             BIGINT NOT NULL,
    data                JSON,
    created_date        TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS users_firebase_registrations
(
    id                    BIGSERIAL PRIMARY KEY,
    registration_id       VARCHAR(200) NOT NULL,
    user_id               BIGINT NOT NULL,
    device_type           BIGINT NOT NULL DEFAULT 0
);