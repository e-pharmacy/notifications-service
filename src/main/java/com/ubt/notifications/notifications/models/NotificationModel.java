package com.ubt.notifications.notifications.models;

import com.ubt.notifications.commons.models.BoBaseModel;

import java.util.Map;

public class NotificationModel extends BoBaseModel {
  private Long userId;
  private Boolean read;
  private Boolean seen;
  private Long typeId;
  //data is different json for each type of notification
  private Map<String, Object> data;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Boolean getRead() {
    return read;
  }

  public void setRead(Boolean read) {
    this.read = read;
  }

  public Boolean getSeen() {
    return seen;
  }

  public void setSeen(Boolean seen) {
    this.seen = seen;
  }

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  public Map<String, Object> getData() {
    return data;
  }

  public void setData(Map<String, Object> data) {
    this.data = data;
  }
}
