package com.ubt.notifications.notifications;

import com.ubt.notifications.commons.models.PageableCustomModel;
import com.ubt.notifications.commons.models.UserFirebaseRegistrationModel;
import com.ubt.notifications.jooq.tables.records.NotificationsRecord;
import com.ubt.notifications.jooq.tables.records.UsersFirebaseRegistrationsRecord;
import com.ubt.notifications.notifications.jooqmappers.NotificationsJooqMapper;
import com.ubt.notifications.notifications.jooqmappers.UsersFirebaseRegistrationsJooqMapper;
import com.ubt.notifications.notifications.models.NotificationModel;
import com.ubt.notifications.validation.ValidationService;
import org.jooq.DSLContext;
import org.jooq.InsertValuesStep3;
import org.jooq.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.ubt.notifications.jooq.Tables.NOTIFICATIONS;
import static com.ubt.notifications.jooq.Tables.USERS_FIREBASE_REGISTRATIONS;

@Repository
public class NotificationsRepositoryImpl implements NotificationsRepository {
  private final DSLContext create;

  @Autowired
  public NotificationsRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public Page<NotificationModel> getNotificationByUserId(Pageable pageable, long userId) {
    PageableCustomModel pageableCustom = new PageableCustomModel(pageable);

    if (ValidationService.isEmpty(pageableCustom.getOrders())) {
      //by default sort
      pageableCustom.addOrders(NOTIFICATIONS.CREATED_DATE.desc());
    }

    long totalElements = create.selectCount().from(NOTIFICATIONS)
            .where(NOTIFICATIONS.USER_ID.eq(userId))
            .fetchOneInto(long.class);

    List<NotificationModel> content = NotificationsJooqMapper.map(
            create.selectFrom(NOTIFICATIONS)
                    .where(NOTIFICATIONS.USER_ID.eq(userId))
                    .orderBy(pageableCustom.getOrders())
                    .offset(pageableCustom.getOffset())
                    .limit(pageableCustom.getLimit())
                    .fetch()
    );

    return new PageImpl<>(content, pageableCustom.getPageable(), totalElements);
  }

  @Override
  public NotificationModel getNotificationById(long id) {
    return NotificationsJooqMapper.map(
            create.selectFrom(NOTIFICATIONS)
                    .where(NOTIFICATIONS.ID.eq(id))
                    .fetchOne()
    );
  }

  @Override
  public NotificationModel addNotification(NotificationModel notification) {
    NotificationsRecord record = NotificationsJooqMapper.unmap(notification);

    return NotificationsJooqMapper.map(
            create.insertInto(NOTIFICATIONS)
                    .set(record)
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public List<NotificationModel> addAllNotifications(List<NotificationModel> notifications) {
    InsertValuesStep3<NotificationsRecord, Long, Long, JSON> insertQuery =
            create.insertInto(NOTIFICATIONS, NOTIFICATIONS.USER_ID, NOTIFICATIONS.TYPE_ID, NOTIFICATIONS.DATA);

    for (NotificationModel notification : notifications) {
      NotificationsRecord record = NotificationsJooqMapper.unmap(notification);
      insertQuery.values(record.getUserId(), record.getTypeId(),
              record.getData());
    }

    return NotificationsJooqMapper.map(
            insertQuery.returning().fetch()
    );
  }

  @Override
  public NotificationModel markRead(long id) {
    return NotificationsJooqMapper.map(
            create.update(NOTIFICATIONS)
                    .set(NOTIFICATIONS.READ, true)
                    .where(NOTIFICATIONS.ID.eq(id))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public NotificationModel deleteNotification(long id) {
    return NotificationsJooqMapper.map(
            create.deleteFrom(NOTIFICATIONS)
                    .where(NOTIFICATIONS.ID.eq(id))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public void markAllRead(long userId) {
    create.update(NOTIFICATIONS)
            .set(NOTIFICATIONS.READ, true)
            .where(NOTIFICATIONS.USER_ID.eq(userId))
            .execute();
  }

  @Override
  public void markSeen(long userId) {
    create.update(NOTIFICATIONS)
            .set(NOTIFICATIONS.SEEN, true)
            .where(NOTIFICATIONS.USER_ID.eq(userId))
            .execute();
  }

  @Override
  public void addFirebaseRegistrationId(UserFirebaseRegistrationModel userFirebaseRegistrationModel) {
    UsersFirebaseRegistrationsRecord record = UsersFirebaseRegistrationsJooqMapper.unmap(userFirebaseRegistrationModel);

    create.insertInto(USERS_FIREBASE_REGISTRATIONS)
            .set(record)
            .execute();
  }

  @Override
  public void removeFirebaseRegistrationId(UserFirebaseRegistrationModel userFirebaseRegistrationModel) {
    create.deleteFrom(USERS_FIREBASE_REGISTRATIONS)
            .where(USERS_FIREBASE_REGISTRATIONS.USER_ID.eq(userFirebaseRegistrationModel.getUserId()))
            .and(USERS_FIREBASE_REGISTRATIONS.REGISTRATION_ID.eq(userFirebaseRegistrationModel.getRegistrationId()))
            .execute();
  }

  @Override
  public UserFirebaseRegistrationModel getRegistrationId(String registrationId) {
    return UsersFirebaseRegistrationsJooqMapper.map(
            create.selectFrom(USERS_FIREBASE_REGISTRATIONS)
                    .where(USERS_FIREBASE_REGISTRATIONS.REGISTRATION_ID.eq(registrationId))
                    .fetchOne()
    );
  }
}