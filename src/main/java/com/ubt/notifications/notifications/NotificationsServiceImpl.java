package com.ubt.notifications.notifications;

import com.ubt.notifications.commons.builders.UserFirebaseRegistrationModelBuilder;
import com.ubt.notifications.commons.models.UserFirebaseRegistrationModel;
import com.ubt.notifications.firebase.FirebaseService;
import com.ubt.notifications.notifications.models.NotificationModel;
import com.ubt.notifications.notificationsdata.NotificationsDataService;
import com.ubt.notifications.utils.UserContextHolder;
import com.ubt.notifications.validation.ValidationService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class NotificationsServiceImpl implements NotificationsService {
  private final NotificationsRepository notificationsRepository;
  private final NotificationsDataService notificationsDataService;
  private final FirebaseService firebaseService;

  @Autowired
  public NotificationsServiceImpl(NotificationsRepository notificationsRepository, NotificationsDataService notificationsDataService, FirebaseService firebaseService) {
    this.notificationsRepository = notificationsRepository;
    this.notificationsDataService = notificationsDataService;
    this.firebaseService = firebaseService;
  }

  @Override
  public Page<NotificationModel> getNotificationByUserId(Pageable pageable, long userId) {
    Page<NotificationModel> page = notificationsRepository.getNotificationByUserId(pageable, userId);
    List<NotificationModel> notifications = page.getContent();

    //get notifications data
    notificationsDataService.getNotificationsData(notifications);

    return new PageImpl<>(notifications, pageable, page.getTotalElements());
  }

  @Override
  public NotificationModel getNotificationById(long id) {
    NotificationModel notificationModel = notificationsRepository.getNotificationById(id);

    //get notification data
    notificationsDataService.getNotificationData(notificationModel);

    return notificationModel;
  }

  @Override
  public NotificationModel addNotification(NotificationModel notification) {
    if (notification == null) {
      return null;
    }
    NotificationModel addedNotification = notificationsRepository.addNotification(notification);

    //get notifications data
    notificationsDataService.getNotificationData(addedNotification);
    //send firebase notification
    firebaseService.sendOne(addedNotification);

    return addedNotification;
  }

  @Override
  public List<NotificationModel> addAllNotifications(List<NotificationModel> notifications) {
    if (ValidationService.isEmpty(notifications)) {
      return null;
    }
    List<NotificationModel> addedNotifications = notificationsRepository.addAllNotifications(notifications);

    //get notifications data
    notificationsDataService.getNotificationsData(addedNotifications);
    //send firebase notifications
    firebaseService.sendMany(addedNotifications);

    return addedNotifications;
  }

  @Override
  public NotificationModel markRead(long id) {
    return notificationsRepository.markRead(id);
  }

  @Override
  public void markAllRead(long userId) {
    notificationsRepository.markAllRead(userId);
  }

  @Override
  public void markSeen(long userId) {
    notificationsRepository.markSeen(userId);
  }

  @Override
  public NotificationModel deleteNotification(long id) {
    return notificationsRepository.deleteNotification(id);
  }

  @Override
  public void addFirebaseRegistrationId(UserFirebaseRegistrationModel userFirebaseRegistrationModel) {
    long userId = UserContextHolder.getContext().getUserId();
    userFirebaseRegistrationModel.setUserId(userId);
    //set userId as notification group name
    userFirebaseRegistrationModel.setNotificationKeyName(String.valueOf(userId));

    UserFirebaseRegistrationModel exists = notificationsRepository.getRegistrationId(userFirebaseRegistrationModel.getRegistrationId());

    //if an id of the same device exists remove it
    if (exists != null) {
      try {
        removeFirebaseRegistrationId(exists.getRegistrationId(), exists.getUserId());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    firebaseService.addRegistrationIds(userFirebaseRegistrationModel);

    notificationsRepository.addFirebaseRegistrationId(userFirebaseRegistrationModel);
  }

  @Override
  public void removeFirebaseRegistrationId(String registrationId) {
    long userId = UserContextHolder.getContext().getUserId();

    removeFirebaseRegistrationId(registrationId, userId);
  }

  private void removeFirebaseRegistrationId(String registrationId, Long userId) {
    UserFirebaseRegistrationModel userFirebaseRegistrationModel = UserFirebaseRegistrationModelBuilder.anUserFirebaseRegistrationModel()
            .withRegistrationId(registrationId)
            .withUserId(userId)
            .build();
    //set userId as notification group name
    userFirebaseRegistrationModel.setNotificationKeyName(String.valueOf(userId));
    firebaseService.removeRegistrationIds(userFirebaseRegistrationModel);

    notificationsRepository.removeFirebaseRegistrationId(userFirebaseRegistrationModel);
  }
}