package com.ubt.notifications.notifications.jooqmappers;

import com.ubt.notifications.commons.DateTimeService;
import com.ubt.notifications.commons.JSONAdapter;
import com.ubt.notifications.jooq.tables.records.NotificationsRecord;
import com.ubt.notifications.notifications.builders.NotificationModelBuilder;
import com.ubt.notifications.notifications.models.NotificationModel;
import org.jooq.JSON;

import java.util.ArrayList;
import java.util.List;

public class NotificationsJooqMapper {
  public static NotificationModel map(NotificationsRecord record) {
    NotificationModel model = null;
    if (record != null) {
      model = NotificationModelBuilder.aNotificationModel()
              .withId(record.getId())
              .withUserId(record.getUserId())
              .withRead(record.getRead())
              .withSeen(record.getSeen())
              .withTypeId(record.getTypeId())
              .withData(JSONAdapter.convert(record.getData() != null ? record.getData().data() : null))
              .withCreatedDate(DateTimeService.toLong(record.getCreatedDate()))
              .build();
    }
    return model;
  }

  public static NotificationsRecord unmap(NotificationModel model) {
    NotificationsRecord record = new NotificationsRecord();

    if (model.getId() != null) {
      record.setId(model.getId());
    }
    if (model.getUserId() != null) {
      record.setUserId(model.getUserId());
    }
    if (model.getRead() != null) {
      record.setRead(model.getRead());
    }
    if (model.getSeen() != null) {
      record.setSeen(model.getSeen());
    }
    if (model.getTypeId() != null) {
      record.setTypeId(model.getTypeId());
    }
    if (model.getData() != null) {
      record.setData(JSON.valueOf(JSONAdapter.convert(model.getData())));
    }

    return record;
  }

  public static List<NotificationModel> map(List<NotificationsRecord> records) {
    List<NotificationModel> recordList = new ArrayList<>();
    for (NotificationsRecord e : records) {
      recordList.add(map(e));
    }
    return recordList;
  }
}
