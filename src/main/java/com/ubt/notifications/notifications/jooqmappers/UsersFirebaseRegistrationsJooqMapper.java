package com.ubt.notifications.notifications.jooqmappers;

import com.ubt.notifications.commons.DateTimeService;
import com.ubt.notifications.commons.JSONAdapter;
import com.ubt.notifications.commons.builders.UserFirebaseRegistrationModelBuilder;
import com.ubt.notifications.commons.models.UserFirebaseRegistrationModel;
import com.ubt.notifications.jooq.tables.records.UsersFirebaseRegistrationsRecord;
import org.jooq.JSON;

import java.util.ArrayList;
import java.util.List;

public class UsersFirebaseRegistrationsJooqMapper {
  public static UserFirebaseRegistrationModel map(UsersFirebaseRegistrationsRecord record) {
    UserFirebaseRegistrationModel model = null;
    if (record != null) {
      model = UserFirebaseRegistrationModelBuilder.anUserFirebaseRegistrationModel()
              .withId(record.getId())
              .withUserId(record.getUserId())
              .withRegistrationId(record.getRegistrationId())
              .withDeviceType(record.getDeviceType())
              .build();
    }
    return model;
  }

  public static UsersFirebaseRegistrationsRecord unmap(UserFirebaseRegistrationModel model) {
    UsersFirebaseRegistrationsRecord record = new UsersFirebaseRegistrationsRecord();

    if (model.getId() != null) {
      record.setId(model.getId());
    }
    if (model.getUserId() != null) {
      record.setUserId(model.getUserId());
    }
    if (model.getRegistrationId() != null) {
      record.setRegistrationId(model.getRegistrationId());
    }
    if (model.getDeviceType() != null) {
      record.setDeviceType(model.getDeviceType());
    }

    return record;
  }

  public static List<UserFirebaseRegistrationModel> map(List<UsersFirebaseRegistrationsRecord> records) {
    List<UserFirebaseRegistrationModel> recordList = new ArrayList<>();
    for (UsersFirebaseRegistrationsRecord e : records) {
      recordList.add(map(e));
    }
    return recordList;
  }
}
