package com.ubt.notifications.notifications;

import com.ubt.notifications.notifications.models.NotificationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("")
public class NotificationsController {
  private final NotificationsService notificationsService;

  @Autowired
  public NotificationsController(NotificationsService notificationsService) {
    this.notificationsService = notificationsService;
  }

  @GetMapping("/users/{userId}")
  public Page<NotificationModel> getNotificationByUserId(Pageable pageable, @PathVariable long userId) {
    return notificationsService.getNotificationByUserId(pageable, userId);
  }

  // regex qualifier to avoid conflict with endpoints that have strings after /notifications/
  @GetMapping("/{id:[\\d]+}")
  public NotificationModel getNotificationById(@PathVariable long id) {
    return notificationsService.getNotificationById(id);
  }

  @PostMapping("")
  @ResponseStatus(HttpStatus.CREATED)
  public NotificationModel addNotification(@RequestBody NotificationModel notification) {
    return notificationsService.addNotification(notification);
  }

  @PostMapping("/all")
  @ResponseStatus(HttpStatus.CREATED)
  public List<NotificationModel> addAllNotifications(@RequestBody List<NotificationModel> notifications) {
    return notificationsService.addAllNotifications(notifications);
  }

  @PutMapping("/{id:[\\d]+}")
  public NotificationModel markRead(@PathVariable long id) {
    return notificationsService.markRead(id);
  }

  @PutMapping("/users/{userId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void markAllRead(@PathVariable long userId) {
    notificationsService.markAllRead(userId);
  }

  @PutMapping("users/{userId}/mark-seen")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void markSeen(@PathVariable long userId) {
    notificationsService.markSeen(userId);
  }

  @DeleteMapping("/{id:[\\d]+}")
  public NotificationModel deleteNotification(@PathVariable long id) {
    return notificationsService.deleteNotification(id);
  }
}