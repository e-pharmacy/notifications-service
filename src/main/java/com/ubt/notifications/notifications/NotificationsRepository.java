package com.ubt.notifications.notifications;

import com.ubt.notifications.commons.models.UserFirebaseRegistrationModel;
import com.ubt.notifications.notifications.models.NotificationModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface NotificationsRepository {
  Page<NotificationModel> getNotificationByUserId(Pageable pageable, long userId);

  NotificationModel getNotificationById(long id);

  NotificationModel addNotification(NotificationModel notification);

  List<NotificationModel> addAllNotifications(List<NotificationModel> notifications);

  NotificationModel markRead(long id);

  NotificationModel deleteNotification(long id);

  void markAllRead(long userId);

  void markSeen(long userId);

  void addFirebaseRegistrationId(UserFirebaseRegistrationModel userFirebaseRegistrationModel);

  void removeFirebaseRegistrationId(UserFirebaseRegistrationModel userFirebaseRegistrationModel);

  UserFirebaseRegistrationModel getRegistrationId(String registrationId);
}