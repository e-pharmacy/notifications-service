package com.ubt.notifications.notifications.builders;

import com.ubt.notifications.notifications.models.NotificationModel;

import java.util.Map;

public final class NotificationModelBuilder {
  private Long userId;
  private Boolean read;
  private Boolean seen;
  private Long typeId;
  //data is different json for each type of notification
  private Map<String, Object> data;
  private Long id;
  private Long createdDate;
  private Long updatedDate;

  private NotificationModelBuilder() {
  }

  public static NotificationModelBuilder aNotificationModel() {
    return new NotificationModelBuilder();
  }

  public NotificationModelBuilder withUserId(Long userId) {
    this.userId = userId;
    return this;
  }

  public NotificationModelBuilder withRead(Boolean read) {
    this.read = read;
    return this;
  }

  public NotificationModelBuilder withSeen(Boolean seen) {
    this.seen = seen;
    return this;
  }

  public NotificationModelBuilder withTypeId(Long typeId) {
    this.typeId = typeId;
    return this;
  }

  public NotificationModelBuilder withData(Map<String, Object> data) {
    this.data = data;
    return this;
  }

  public NotificationModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public NotificationModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public NotificationModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public NotificationModel build() {
    NotificationModel notificationModel = new NotificationModel();
    notificationModel.setUserId(userId);
    notificationModel.setRead(read);
    notificationModel.setSeen(seen);
    notificationModel.setTypeId(typeId);
    notificationModel.setData(data);
    notificationModel.setId(id);
    notificationModel.setCreatedDate(createdDate);
    notificationModel.setUpdatedDate(updatedDate);
    return notificationModel;
  }
}
