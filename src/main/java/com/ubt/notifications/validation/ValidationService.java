package com.ubt.notifications.validation;

import java.util.Collection;

public final class ValidationService {
  public static boolean notBlank(String input) {
    return input != null && !input.trim().isEmpty();
  }

  public static boolean notEmpty(Collection<?> input) {
    return input != null && !input.isEmpty();
  }

  public static boolean isEmpty(Collection<?> input) {
    return !notEmpty(input);
  }
}
