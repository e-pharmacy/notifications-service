package com.ubt.notifications;

import com.ubt.notifications.commons.models.FirebaseConfigurationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(FirebaseConfigurationProperties.class)
@SpringBootApplication
public class NotificationsServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(NotificationsServiceApplication.class, args);
  }

}
