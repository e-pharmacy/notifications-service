package com.ubt.notifications.mailsender;

import com.ubt.notifications.mailsender.models.MailModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/mail")
public class MailSenderController {
  private final MailSenderService mailSenderService;

  @Autowired
  public MailSenderController(MailSenderService mailSenderService) {
    this.mailSenderService = mailSenderService;
  }

  @PostMapping("/send/test")
  public void sendTest(@RequestBody MailModel mailModel) {
    mailSenderService.sendTest(mailModel);
  }

  @PostMapping("/send")
  public void send(@RequestBody MailModel mail) {
    mailSenderService.send(mail);
  }

  @PostMapping("/send/all")
  public void sendAll(@RequestBody List<MailModel> mails) {
    mailSenderService.sendAll(mails);
  }
}
