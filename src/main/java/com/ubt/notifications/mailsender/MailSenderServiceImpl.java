package com.ubt.notifications.mailsender;

import com.ubt.notifications.exceptions.BadRequestException;
import com.ubt.notifications.mailsender.models.MailModel;
import com.ubt.notifications.validation.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import java.util.List;

@Service
@EnableAutoConfiguration
public class MailSenderServiceImpl implements MailSenderService {
  private final JavaMailSender emailSender;

  @Autowired
  public MailSenderServiceImpl(JavaMailSender emailSender) {
    this.emailSender = emailSender;
  }

  @Override
  public void sendTest(MailModel mail) {
    if (ValidationService.notBlank(mail.getRecipient())) {
      sendMail(mail.getRecipient(), "Testing email", "this is a testing email", false);
    }
  }

  public void send(MailModel mail) {
    //send mail to a single recipient
    if (ValidationService.notBlank(mail.getRecipient())) {
      sendMail(mail.getRecipient(), mail.getSubject(), mail.getBody(), mail.isHtml());
    }
    //send the same mail to multiple recipients
    else if (ValidationService.notEmpty(mail.getRecipients())) {
      for (String recipient : mail.getRecipients()) {
        sendMail(recipient, mail.getSubject(), mail.getBody(), mail.isHtml());
      }
    } else {
      throw new BadRequestException("Invalid recipients, single recipient or list of recipients must be provided");
    }
  }

  @Override
  public void sendAll(List<MailModel> mails) {
    //send different emails for each different recipient
    for (MailModel mail : mails) {
      sendMail(mail.getRecipient(), mail.getSubject(), mail.getBody(), mail.isHtml());
    }
  }

  private void sendMail(String recipient, String subject, String body, boolean html) {
    MimeMessagePreparator messagePreparator = mimeMessage -> {
      MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
      messageHelper.setTo(recipient);
      messageHelper.setSubject(subject);
      messageHelper.setFrom(new InternetAddress("xxx@gmail.com", "app"));
      messageHelper.setText(body, html);
    };
    emailSender.send(messagePreparator);
  }
}
