package com.ubt.notifications.mailsender;

import com.ubt.notifications.mailsender.models.MailModel;

import java.util.List;

public interface MailSenderService {
  void sendTest(MailModel mailModel);

  void send(MailModel mail);

  void sendAll(List<MailModel> mails);
}
