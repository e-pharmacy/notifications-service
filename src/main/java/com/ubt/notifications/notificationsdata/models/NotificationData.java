package com.ubt.notifications.notificationsdata.models;

import com.ubt.notifications.notifications.models.NotificationModel;

import java.util.List;

public abstract class NotificationData {
  //static attribute names for notifications data

  /**
   * adds the right data to notification based on its type
   *
   * @param notification notification model
   */
  public abstract void addData(NotificationModel notification);

  /**
   * adds the right data to notifications based on its type
   *
   * @param notification list of notifications with the same type
   */
  public abstract void addData(List<NotificationModel> notification);
}
