package com.ubt.notifications.notificationsdata;

import com.ubt.notifications.notifications.models.NotificationModel;
import com.ubt.notifications.notificationsdata.models.NotificationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class to get notifications data from other services
 */
@Service
public class NotificationsDataService {
  private final NotificationDataFactory notificationDataFactory;

  @Autowired
  public NotificationsDataService(NotificationDataFactory notificationDataFactory) {
    this.notificationDataFactory = notificationDataFactory;
  }

  public void getNotificationData(NotificationModel notification) {
    Map<String, Object> data = notification.getData();
    if (data == null) return;

    NotificationData notificationData = notificationDataFactory.create(notification.getTypeId());

    if (notificationData != null) {
      notificationData.addData(notification);
    }
  }

  public void getNotificationsData(List<NotificationModel> notifications) {
    Map<Long, List<NotificationModel>> groupedByType = notifications.stream().collect(Collectors.groupingBy(NotificationModel::getTypeId));

    groupedByType.forEach((key, values) -> {
              NotificationData notificationData = notificationDataFactory.create(key);
              if (notificationData != null) {
                notificationData.addData(values);
              }
            }
    );
  }
}
