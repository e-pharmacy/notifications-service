package com.ubt.notifications.notificationsdata;

import com.ubt.notifications.notifications.models.NotificationModel;
import com.ubt.notifications.notificationsdata.models.NotificationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationDataFactory {

  /**
   * creates instances of different notificationData objects types based on type id
   *
   * @param notificationType
   * @return
   */
  public NotificationData create(Long notificationType) {
    return new NotificationData() {
      @Override
      public void addData(NotificationModel notification) {

      }

      @Override
      public void addData(List<NotificationModel> notification) {

      }
    };
  }
}
