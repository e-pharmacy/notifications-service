package com.ubt.notifications.firebase;

import com.ubt.notifications.commons.MapConverter;
import com.ubt.notifications.commons.builders.FirebaseDataModelBuilder;
import com.ubt.notifications.commons.builders.FirebaseDeviceGroupDataModelBuilder;
import com.ubt.notifications.commons.models.*;
import com.ubt.notifications.notifications.models.NotificationModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service
public class FirebaseServiceImpl implements FirebaseService {
  private static final Logger logger = LoggerFactory.getLogger("App");
  private final RestTemplate restTemplate;
  private final FirebaseConfigurationProperties firebaseConfigurationProperties;
  @Value("${server-address}")
  private String serverAddress;
  @Value("${firebase.api-url}")
  private String firebaseUrl;

  @Autowired
  public FirebaseServiceImpl(RestTemplate restTemplate, FirebaseConfigurationProperties firebaseConfigurationProperties) {
    this.restTemplate = restTemplate;
    this.firebaseConfigurationProperties = firebaseConfigurationProperties;
  }

  @Override
  public void addRegistrationIds(UserFirebaseRegistrationModel registrationModel) {
    FirebaseDeviceGroupDataModel deviceGroupDataModel = getDeviceGroupData(registrationModel.getNotificationKeyName());

    FirebaseDeviceGroupDataModel deviceGroup = FirebaseDeviceGroupDataModelBuilder.aFirebaseDeviceGroupDataModel()
            .withOperation("create")
            .withNotificationKeyName(registrationModel.getNotificationKeyName())
            .withRegistrationIds(List.of(registrationModel.getRegistrationId()))
            .build();

    if (deviceGroupDataModel != null) {
      deviceGroup.setOperation("add");
      deviceGroup.setNotificationKey(deviceGroupDataModel.getNotificationKey());
    }
    restTemplate.exchange(firebaseUrl + "/notification",
            HttpMethod.POST,
            new HttpEntity<>(deviceGroup, getHeaders()),
            FirebaseDeviceGroupDataModel.class);
  }

  @Override
  public void removeRegistrationIds(UserFirebaseRegistrationModel registrationModel) {
    FirebaseDeviceGroupDataModel deviceGroupDataModel = getDeviceGroupData(registrationModel.getNotificationKeyName());

    if (deviceGroupDataModel != null) {
      FirebaseDeviceGroupDataModel deviceGroup = FirebaseDeviceGroupDataModelBuilder.aFirebaseDeviceGroupDataModel()
              .withOperation("remove")
              .withNotificationKeyName(registrationModel.getNotificationKeyName())
              .withNotificationKey(deviceGroupDataModel.getNotificationKey())
              .withRegistrationIds(List.of(registrationModel.getRegistrationId()))
              .build();

      restTemplate.exchange(firebaseUrl + "/notification",
              HttpMethod.POST,
              new HttpEntity<>(deviceGroup, getHeaders()),
              FirebaseDeviceGroupDataModel.class);
    }
  }

  @Async
  @Override
  public void sendMany(List<NotificationModel> notifications) {
    try {
      for (NotificationModel notification : notifications) {
        send(notification);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Async
  @Override
  public void sendOne(NotificationModel notificationModel) {
    try {
      send(notificationModel);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void send(NotificationModel notificationModel) {
    String groupName = String.valueOf(notificationModel.getUserId());

    FirebaseDeviceGroupDataModel deviceGroup = getDeviceGroupData(groupName);

    if (deviceGroup != null) {
      FirebaseDataModel firebaseDataModel = createFirebaseModel(notificationModel, deviceGroup);

      FirebaseResponseModel firebaseResponse;
      try {
        firebaseResponse = restTemplate.exchange(firebaseUrl + "/send",
                HttpMethod.POST,
                new HttpEntity<>(firebaseDataModel, getHeaders()),
                FirebaseResponseModel.class)
                .getBody();
      } catch (Exception e) {
        firebaseResponse = null;
        logger.error("Could not send notification to firebase, nested exception is: " + e);
      }

      //if there are failed ids try to resend notification to those ids
      if (firebaseResponse != null && firebaseResponse.getFailure() > 0) {
        //remove group key from firebase model and
        //add specific failed registration ids to retry only those ids
        firebaseDataModel.setTo(null);
        firebaseDataModel.setRegistrationIds(firebaseResponse.getFailedRegistrationIds());

        try {
          //wait 5 seconds before retrying
          Thread.sleep(5000);

          restTemplate.exchange(firebaseUrl + "/send",
                  HttpMethod.POST,
                  new HttpEntity<>(firebaseDataModel, getHeaders()),
                  FirebaseResponseModel.class);
        } catch (Exception e) {
          logger.error("Could not resend notification to firebase, nested exception is: " + e);
        }
      }
    }
  }

  private HttpHeaders getHeaders() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set("Authorization", "key=" + firebaseConfigurationProperties.getServerKey());
    headers.set("project_id", firebaseConfigurationProperties.getSenderId());

    return headers;
  }

  private FirebaseDataModel createFirebaseModel(NotificationModel notificationModel, FirebaseDeviceGroupDataModel deviceGroup) {
    Map<String, Object> data = MapConverter.convert(notificationModel);

    Map<String, Object> notificationData = notificationModel.getData();
    Long type = notificationModel.getTypeId();
    String url = serverAddress;
    Notification notification = null;

    data.put("url", url);

    return FirebaseDataModelBuilder.aFirebaseDataModel()
            .withTo(deviceGroup.getNotificationKey())
            .withData(data)
            .withNotification(notification)
            .build();
  }

  private FirebaseDeviceGroupDataModel getDeviceGroupData(String groupName) {
    HttpHeaders headers = getHeaders();
    HttpEntity<FirebaseDeviceGroupDataModel> entity = new HttpEntity<>(null, headers);

    FirebaseDeviceGroupDataModel deviceGroup;
    try {
      deviceGroup = restTemplate.exchange(firebaseUrl + "/notification?notification_key_name=" + groupName,
              HttpMethod.GET, entity, FirebaseDeviceGroupDataModel.class).getBody();

      if (deviceGroup != null) {
        deviceGroup.setNotificationKeyName(groupName);
      }
    } catch (Exception e) {
      deviceGroup = null;
    }
    return deviceGroup;
  }
}