package com.ubt.notifications.firebase;

import com.ubt.notifications.commons.models.UserFirebaseRegistrationModel;
import com.ubt.notifications.notifications.models.NotificationModel;

import java.util.List;

public interface FirebaseService {
  void addRegistrationIds(UserFirebaseRegistrationModel userFirebaseRegistrationModel);

  void removeRegistrationIds(UserFirebaseRegistrationModel userFirebaseRegistrationModel);

  void sendMany(List<NotificationModel> notifications);

  void sendOne(NotificationModel notificationModel);
}
