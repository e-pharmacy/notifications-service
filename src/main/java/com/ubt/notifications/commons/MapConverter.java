package com.ubt.notifications.commons;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.ubt.notifications.notifications.models.NotificationModel;

import java.util.Map;

public class MapConverter {
  /**
   * converts json String to Map
   *
   * @param data data to be converted
   * @return converted Map
   */
  public static Map<String, Object> convert(String data) {
    ObjectMapper objectMapper = new ObjectMapper();
    Map<String, Object> json;
    try {
      if (data == null) {
        return null;
      }
      json = objectMapper.readValue(data, Map.class);
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Cannot convert json string to Map<String, Object>");
    }
    return json;
  }

  /**
   * converts Map to json String
   *
   * @param data data to be converted
   * @return converted json String
   */
  public static String convert(Map<String, Object> data) {
    ObjectMapper objectMapper = new ObjectMapper();
    String json;
    try {
      if (data == null) {
        return null;
      }
      json = objectMapper.writeValueAsString(data);
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Cannot convert Map<String, Object> to json string");
    }
    return json;
  }

  /**
   * converts NotificationModel to Map
   *
   * @param notificationModel data to be converted
   * @return converted json String
   */
  public static Map<String, Object> convert(NotificationModel notificationModel) {
    ObjectMapper objectMapper = new ObjectMapper();

    //set naming strategy to snake case to converted attributes
    objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

    // Convert POJO to Map
    return objectMapper.convertValue(notificationModel, new TypeReference<>() {});
  }

  public static Object convertMapToPojo(Map<String, Object> map, Object pojo){
    final ObjectMapper mapper = new ObjectMapper();
    return mapper.convertValue(map, pojo.getClass());
  }
}
