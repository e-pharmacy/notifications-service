package com.ubt.notifications.commons;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestTemplateBeanBuilder {

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }
}
