package com.ubt.notifications.commons;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

public class JSONAdapter {
  /**
   * converts json String to Map
   *
   * @param data data to be converted
   * @return converted Map
   */
  public static Map<String, Object> convert(String data) {
    ObjectMapper objectMapper = new ObjectMapper();
    Map<String, Object> json;
    try {
      if (data == null) {
        return null;
      }
      json = objectMapper.readValue(data, Map.class);
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Cannot convert json string to Map<String, Object>");
    }
    return json;
  }

  /**
   * converts Map to json String
   *
   * @param data data to be converted
   * @return converted json String
   */
  public static String convert(Map<String, Object> data) {
    ObjectMapper objectMapper = new ObjectMapper();
    String json;
    try {
      if (data == null) {
        return null;
      }
      json = objectMapper.writeValueAsString(data);
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Cannot convert Map<String, Object> to json string");
    }
    return json;
  }
}
