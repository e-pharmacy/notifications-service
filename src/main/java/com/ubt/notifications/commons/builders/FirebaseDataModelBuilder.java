package com.ubt.notifications.commons.builders;

import com.ubt.notifications.commons.models.FirebaseDataModel;
import com.ubt.notifications.commons.models.Notification;

import java.util.List;
import java.util.Map;

public final class FirebaseDataModelBuilder {
  private String to;
  private List<String> registrationIds;
  private Notification notification;
  private Map<String, Object> data;

  private FirebaseDataModelBuilder() {
  }

  public static FirebaseDataModelBuilder aFirebaseDataModel() {
    return new FirebaseDataModelBuilder();
  }

  public FirebaseDataModelBuilder withTo(String to) {
    this.to = to;
    return this;
  }

  public FirebaseDataModelBuilder withRegistrationIds(List<String> registrationIds) {
    this.registrationIds = registrationIds;
    return this;
  }

  public FirebaseDataModelBuilder withNotification(Notification notification) {
    this.notification = notification;
    return this;
  }

  public FirebaseDataModelBuilder withData(Map<String, Object> data) {
    this.data = data;
    return this;
  }

  public FirebaseDataModel build() {
    FirebaseDataModel firebaseDataModel = new FirebaseDataModel();
    firebaseDataModel.setTo(to);
    firebaseDataModel.setRegistrationIds(registrationIds);
    firebaseDataModel.setNotification(notification);
    firebaseDataModel.setData(data);
    return firebaseDataModel;
  }
}
