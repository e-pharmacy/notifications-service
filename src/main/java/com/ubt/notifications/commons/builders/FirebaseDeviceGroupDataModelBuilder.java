package com.ubt.notifications.commons.builders;

import com.ubt.notifications.commons.models.FirebaseDeviceGroupDataModel;

import java.util.List;

public final class FirebaseDeviceGroupDataModelBuilder {
  private String operation;
  private String notificationKeyName;
  private String notificationKey;
  private List<String> registrationIds;

  private FirebaseDeviceGroupDataModelBuilder() {
  }

  public static FirebaseDeviceGroupDataModelBuilder aFirebaseDeviceGroupDataModel() {
    return new FirebaseDeviceGroupDataModelBuilder();
  }

  public FirebaseDeviceGroupDataModelBuilder withOperation(String operation) {
    this.operation = operation;
    return this;
  }

  public FirebaseDeviceGroupDataModelBuilder withNotificationKeyName(String notificationKeyName) {
    this.notificationKeyName = notificationKeyName;
    return this;
  }

  public FirebaseDeviceGroupDataModelBuilder withNotificationKey(String notificationKey) {
    this.notificationKey = notificationKey;
    return this;
  }

  public FirebaseDeviceGroupDataModelBuilder withRegistrationIds(List<String> registrationIds) {
    this.registrationIds = registrationIds;
    return this;
  }

  public FirebaseDeviceGroupDataModel build() {
    FirebaseDeviceGroupDataModel firebaseDeviceGroupDataModel = new FirebaseDeviceGroupDataModel();
    firebaseDeviceGroupDataModel.setOperation(operation);
    firebaseDeviceGroupDataModel.setNotificationKeyName(notificationKeyName);
    firebaseDeviceGroupDataModel.setNotificationKey(notificationKey);
    firebaseDeviceGroupDataModel.setRegistrationIds(registrationIds);
    return firebaseDeviceGroupDataModel;
  }
}
