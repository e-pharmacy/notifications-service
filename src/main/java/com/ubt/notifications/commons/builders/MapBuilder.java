package com.ubt.notifications.commons.builders;

import java.util.HashMap;
import java.util.Map;

/**
 * A Map builder used to call chaining put method
 */
public final class MapBuilder {
  private Map<String, Object> map;

  private MapBuilder() {
    map = new HashMap<>();
  }

  private MapBuilder(Map<String, Object> map) {
    this.map = map;
  }

  public static MapBuilder aMap() {
    return new MapBuilder();
  }

  public static MapBuilder aMap(Map<String, Object> map) {
    return new MapBuilder(map);
  }

  public MapBuilder put(String key, Object value) {
    map.put(key, value);
    return this;
  }

  public Map<String, Object> build() {
    return map;
  }
}
