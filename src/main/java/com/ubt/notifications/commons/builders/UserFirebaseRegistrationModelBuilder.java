package com.ubt.notifications.commons.builders;

import com.ubt.notifications.commons.models.UserFirebaseRegistrationModel;

public final class UserFirebaseRegistrationModelBuilder {
  private Long id;
  private Long userId;
  private String registrationId;
  private Long deviceType;
  private String notificationKeyName;

  private UserFirebaseRegistrationModelBuilder() {
  }

  public static UserFirebaseRegistrationModelBuilder anUserFirebaseRegistrationModel() {
    return new UserFirebaseRegistrationModelBuilder();
  }

  public UserFirebaseRegistrationModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public UserFirebaseRegistrationModelBuilder withUserId(Long userId) {
    this.userId = userId;
    return this;
  }

  public UserFirebaseRegistrationModelBuilder withRegistrationId(String registrationId) {
    this.registrationId = registrationId;
    return this;
  }

  public UserFirebaseRegistrationModelBuilder withDeviceType(Long deviceType) {
    this.deviceType = deviceType;
    return this;
  }

  public UserFirebaseRegistrationModelBuilder withNotificationKeyName(String notificationKeyName) {
    this.notificationKeyName = notificationKeyName;
    return this;
  }

  public UserFirebaseRegistrationModel build() {
    UserFirebaseRegistrationModel userFirebaseRegistrationModel = new UserFirebaseRegistrationModel();
    userFirebaseRegistrationModel.setId(id);
    userFirebaseRegistrationModel.setUserId(userId);
    userFirebaseRegistrationModel.setRegistrationId(registrationId);
    userFirebaseRegistrationModel.setDeviceType(deviceType);
    userFirebaseRegistrationModel.setNotificationKeyName(notificationKeyName);
    return userFirebaseRegistrationModel;
  }
}
