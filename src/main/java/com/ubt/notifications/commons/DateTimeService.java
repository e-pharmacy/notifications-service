package com.ubt.notifications.commons;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;

public class DateTimeService {
  public static Long toLong(LocalDateTime localDateTime) {
    if (localDateTime != null) {
      return localDateTime.atZone(ZoneId.systemDefault())
              .toInstant().toEpochMilli();
    }
    return null;
  }

  public static LocalDateTime toLocalDateTime(Long timestamp) {
    if (timestamp != null) {
      return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp),
              TimeZone.getDefault().toZoneId());
    }
    return null;
  }
}
