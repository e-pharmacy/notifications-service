package com.ubt.notifications.commons.models;

import java.util.List;

public class FirebaseResponseModel {
  private int success;
  private int failure;
  private List<String> failedRegistrationIds;

  public int getSuccess() {
    return success;
  }

  public void setSuccess(int success) {
    this.success = success;
  }

  public int getFailure() {
    return failure;
  }

  public void setFailure(int failure) {
    this.failure = failure;
  }

  public List<String> getFailedRegistrationIds() {
    return failedRegistrationIds;
  }

  public void setFailedRegistrationIds(List<String> failedRegistrationIds) {
    this.failedRegistrationIds = failedRegistrationIds;
  }
}
