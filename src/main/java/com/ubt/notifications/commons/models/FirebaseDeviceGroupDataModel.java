package com.ubt.notifications.commons.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class FirebaseDeviceGroupDataModel {
  private String operation;
  @JsonProperty("notification_key_name")
  private String notificationKeyName;
  @JsonProperty("notification_key")
  private String notificationKey;
  @JsonProperty("registration_ids")
  private List<String> registrationIds;

  public String getOperation() {
    return operation;
  }

  public void setOperation(String operation) {
    this.operation = operation;
  }

  public String getNotificationKeyName() {
    return notificationKeyName;
  }

  public void setNotificationKeyName(String notificationKeyName) {
    this.notificationKeyName = notificationKeyName;
  }

  public String getNotificationKey() {
    return notificationKey;
  }

  public void setNotificationKey(String notificationKey) {
    this.notificationKey = notificationKey;
  }

  public List<String> getRegistrationIds() {
    return registrationIds;
  }

  public void setRegistrationIds(List<String> registrationIds) {
    this.registrationIds = registrationIds;
  }
}
