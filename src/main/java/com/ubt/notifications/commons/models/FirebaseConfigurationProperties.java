/**
 * Created by Muhamed Vrajolli.
 */
package com.ubt.notifications.commons.models;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "firebase")
public class FirebaseConfigurationProperties {
  private String serverKey;
  private String projectId;
  private String androidAppId;
  private String iosAppId;
  private String webAppId;
  private String senderId;
  private String apiKey;

  public String getServerKey() {
    return serverKey;
  }

  public void setServerKey(String serverKey) {
    this.serverKey = serverKey;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getAndroidAppId() {
    return androidAppId;
  }

  public void setAndroidAppId(String androidAppId) {
    this.androidAppId = androidAppId;
  }

  public String getIosAppId() {
    return iosAppId;
  }

  public void setIosAppId(String iosAppId) {
    this.iosAppId = iosAppId;
  }

  public String getWebAppId() {
    return webAppId;
  }

  public void setWebAppId(String webAppId) {
    this.webAppId = webAppId;
  }

  public String getSenderId() {
    return senderId;
  }

  public void setSenderId(String senderId) {
    this.senderId = senderId;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
}
