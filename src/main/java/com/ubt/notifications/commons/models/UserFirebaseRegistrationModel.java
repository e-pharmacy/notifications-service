package com.ubt.notifications.commons.models;

public class UserFirebaseRegistrationModel {
  private Long id;
  private Long userId;
  private String registrationId;
  private Long deviceType;
  private String notificationKeyName;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getRegistrationId() {
    return registrationId;
  }

  public void setRegistrationId(String registrationId) {
    this.registrationId = registrationId;
  }

  public Long getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(Long deviceType) {
    this.deviceType = deviceType;
  }

  public String getNotificationKeyName() {
    return notificationKeyName;
  }

  public void setNotificationKeyName(String notificationKeyName) {
    this.notificationKeyName = notificationKeyName;
  }
}
