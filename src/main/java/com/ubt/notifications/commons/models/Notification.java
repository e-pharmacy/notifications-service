package com.ubt.notifications.commons.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Notification {
  private String body;
  private String title;
  @JsonProperty("click_action")
  private String clickAction;

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getClickAction() {
    return clickAction;
  }

  public void setClickAction(String clickAction) {
    this.clickAction = clickAction;
  }
}
