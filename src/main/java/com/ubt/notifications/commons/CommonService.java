package com.ubt.notifications.commons;

import org.jooq.SortField;
import org.jooq.SortOrder;
import org.jooq.impl.DSL;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

public class CommonService {
  public static List<SortField<?>> convertSortFields(Sort sort) {
    List<SortField<?>> orders = new ArrayList<>();
    //add every element from sort to orders
    for (Sort.Order order : sort) {
      orders.add(DSL.field(order.getProperty()).sort(order.getDirection() == Sort.Direction.ASC ? SortOrder.ASC : SortOrder.DESC));
    }
    return orders;
  }

  /**
   * get long value from Integer object
   *
   * @param object
   * @return
   */
  public static Long getLong(Object object) {
    if (object == null) return null;
    return ((Integer) object).longValue();
  }
}
